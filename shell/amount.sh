#!/bin/bash
#Auto mounter script

DEVICES=$(ls /dev | grep sd[b-z][1-10])
FSLIST=(vfat ntfs ext3)

touch /scripts/.list.tmp
for DEVICE in $DEVICES
do
   for FS in $FSLIST
   do
	mkdir -p /media/$DEVICE
	mount -t $FS /dev/$DEVICE /media/$DEVICE && echo $DEVICE >> /scripts/.list.tmp
   	clear
   done
done

LIST=$(cat /scripts/.list.tmp)
for ELEMENT in $LIST
do
   echo $ELEMENT 'has been successfuly mounted to /media/'$ELEMENT
done

rm -rf /scripts/.*.tmp

exit 0
