#!/bin/bash
#Auto unmounter script

DEVICES=$(ls /media | grep sd[b-z][1-10])

for DEVICE in $DEVICES
do
    umount /media/$DEVICE && rmdir /media/$DEVICE && echo $DEVICE' has been sucessfully unmounted from /media/'$DEVICE
done
exit 0
