#!/bin/bash
# Backup script
# Programmed by bender.rodriges. (c)2008
# Other scripts can be found at http://www.rodriges.org/

# Configuration part starts here

    #Enter path to dir that will be backed up (e.g. /home/mike/documents)
    BACKING_UP_FROM=
    
    #Enter path to dir in which will be backed up (e.g. /home/mike/backups)
    BACKING_UP_TO=

    #Your username in system. You will be an owner of this backup. (e.g. mike)
    USER=

# End of configuration part
# Script body. (Do not change here anything unless you know what)

mkdir -p $BACKING_UP_TO               						# creating backup dir if doesn't exist
tar -zcf $BACKING_UP_TO/backup_`date +%Y%m%d-%k:%M:%S`.tar.gz $BACKING_UP_FROM	# backing up dir
chown -R $USER $BACKING_UP_TO							# setting owner to system user
chmod -R 777 $BACKING_UP_TO
CODE=$?

if [ $CODE -eq 0 ]
 then 
   {
	echo "Backup archive has been successfuly created with name backup_`date +%Y%m%d-%k:%M:%S`.tar.gz by $USER in $BACKING_UP_TO at `date +%Y-%m-%d-%k:%M:%S`" >> $BACKING_UP_TO/backup.log
	echo "------------------------------------------" >> $BACKING_UP_TO/backup.log 
   }
else
   {
	echo "Unexpected error occurres while backing up archive at `date +%Y-%m-%d-%k:%M:%S` by $USER in $BACKING_UP_TO . File has not been created." >> $BACKING_UP_TO/backup.log
	echo "------------------------------------------" >> $BACKING_UP_TO/backup.log
   }
fi

#End of script
