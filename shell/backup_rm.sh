#!/bin/bash
# Backup remove script
# Programmed by bender.rodriges. (c)2008
# Other scripts can be found at http://www.rodriges.org/

# Configuration part starts here

	# Enter path to backups dir
	# e.g. /home/mike/backups
	PATH_TO_BACKUPS=

	# Enter how old must be the file (in days)
	# e.g. Yesterday's file is 1 day old
	OLDER_THEN=

# End of configuration part
# Script body. (Do not change here anything unless you know what)

$OLDER_THEN=`expr $OLDER_THEN - 1`
find ${PATH_TO_BACKUPS}/backup_*.tar.gz -mtime $OLDER_THEN -type f -exec rm -f {} \; || {
	echo "Unexpected error occurres while deleting backup archives older than $OLDER_THEN day(s) at `date +%Y-%m-%d-%k:%M:%S`" >> $PATH_TO_BACKUPS/backup.log
	echo "------------------------------------------" >> $PATH_TO_BACKUPS/backup.log
	exit 1
}

echo "All backup archives older than $OLDER_THEN day(s) has been successfuly deleted at `date +%Y-%m-%d-%k:%M:%S`" >> $PATH_TO_BACKUPS/backup.log
echo "------------------------------------------" >> $PATH_TO_BACKUPS/backup.log
exit 0

# End of script.