# monit monitoring http://mmonit.com/monit/
# www.fduran.com

apt-get update; apt-get install monit

cp /etc/monit/monitrc /etc/monit/monitrc.orig
# edit /etc/monit/monitrc see examples at http://mmonit.com/wiki/Monit/ConfigurationExamples

# Respawn:

# a) systems with /etc/inittab (System-V):
# append to /etc/inittab: 
mo:2345:respawn:/usr/sbin/monit -Ic /etc/monit/monitrc

# restart, after changing /etc/monit/monitrc for example
pkill monit; telinit q

# start/stop
/etc/init.d/monit start|stop

# b) Ubuntu upstart: create /etc/init/monit.conf with:

# Run monit in standard run-levels
start on stopped rc2
start on stopped rc3
start on stopped rc4
start on stopped rc5
stop on runlevel 0
stop on runlevel 1
stop on runlevel 6
respawn
exec /usr/sbin/monit -c /etc/monit/monitrc

# reload upstart configuration
initctl reload-configuration

# start/stop:
start monit
stop monit

# config file /etc/monit/monitrc
# mysql: may not have /var/run/mysqld/mysql.pid , create in my.cf --pid-file= file_name
# upstart systems: start program = "/sbin/start apache2"
# check for errors with /usr/sbin/monit -Ic /etc/monit/monitrc

set daemon  200
set alert youremail@example.com
set httpd port 2812 and
     allow 0.0.0.0/0.0.0.0
     allow admin:somepassword
check system example.com
    if loadavg (1min) > 4 then alert
    if loadavg (5min) > 2 then alert
    if memory usage > 75% then alert
    if cpu usage (user) > 70% then alert
    if cpu usage (system) > 30% then alert
    if cpu usage (wait) > 20% then alert
check process apache with pidfile /var/run/apache2.pid
    start program = "/etc/init.d/apache2 start"
    stop program  = "/etc/init.d/apache2 stop"
    if cpu > 60% for 2 cycles then alert
    if cpu > 80% for 5 cycles then restart
    if failed host example.com port 80 protocol http
       then restart
check device datafs with path /dev/xvda1
     if space usage > 80% for 5 times within 15 cycles then alert