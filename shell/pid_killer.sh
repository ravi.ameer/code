#!/bin/bash
# Process killer script
# Programmed by bender.rodriges. (c) 2008
# Other scripts can be found at http://www.rodriges.org/


    PROCESS_NAME=$(zenity --entry --text "Enter a name of process you want to kill" --entry-text "e.g. firefox")
    	if [ $? != 0 ]
	then 
		zenity --error --text "Script terminated!"
		exit 1
	fi


ps aux | grep $PROCESS_NAME | awk '{print $2, $11}' | tee /home/$USER/killer.tmp

{
echo "--------------------------------"
echo "PID      |      PROCESS"
echo ""
echo "Please remember PID of process needs to be killed and press Close"
} >> /home/$USER/killer.tmp

	
	zenity --text-info --title=test --filename=/home/$USER/killer.tmp
    	if [ $? != 0 ]
	then 
		zenity --error --text "Script terminated!"
		exit 1
	fi

rm -f /home/$USER/killer.tmp

	PROCESS_PID=$(zenity --entry --text "Enter a PID of process" --entry-text "")
    	if [ $? != 0 ]
	then 
		zenity --error --text "Script terminated!"
		exit 1
	fi

kill $PROCESS_PID
if [ $? != 0 ]
then 
	zenity --error --text "You don't have permissions to terminate this process!"
	exit 1
fi

zenity --info --text "Process has been successfuly killed!"
exit 0

#End of script
