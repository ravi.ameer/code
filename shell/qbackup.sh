#!/bin/bash
# Quick backup script

	FILE=$(zenity --file-selection --title="Double-click file to backup")
	if [ $? != 0 ]
	then
		zenity --error --text="Script terminated!"
		exit 1
	fi

	cp $FILE{,.bak}
	if [ $? = 0 ]
	then	
		exit 0
	else
		zenity --error --text="Unexpected error occures while backing up file!"
		exit 1
	fi
