#!/bin/bash
#Cron ping script

	terminate() 
	{
	zenity --error --text "Script terminated!"
	exit 1; 
	}

	if [ "$USER" != "root" ] 
	then 
		zenity --error --text "You are not administrator. Please log in as root and try again."
		exit 1
	fi

	MINUTE=$(zenity --scale --text "Pick a minute (-1 for each) when script will be launched" --min-value=-1 --max-value=59 --value=0 --step 1)
		if [ $? != 0 ] 
		then 
			terminate 
		fi 
		if [ $MINUTE = "-1" ] 
		then 
			MINUTE=* 
		fi
	HOUR=$(zenity --scale --text "Pick an hour (-1 for each) when script will be launched" --min-value=-1 --max-value=23 --value=0 --step 1)
		if [ $? != 0 ] 
		then 
			terminate 
		fi
		if [ $HOUR = "-1" ] 
		then 
			HOUR=* 
		fi
	DAY=$(zenity --scale --text "Pick day of month (-1 for each) when script will be launched" --min-value=-1 --max-value=31 --value=0 --step 1)
		if [ $? != 0 ] 
		then 
			terminate 
		fi
		if [ $DAY = "-1" ] 
		then 
			DAY=* 
		fi

	cd /etc/cron.d/
	CRONTAB_PATH=/etc/cron.d/$(ls -1 | grep cron)
	echo $CRONTAB_PATH

	IP_ADDR=$(zenity --entry --text "Enter pinging system IP or domain" --entry-text "rodriges.org")
		if [ $? != 0 ] 
		then 
			terminate 
		fi
	SCRIPT_PATH=/sbin/server_ping

#BASIC SCRIPT (GENERATE EXEC SCRIPT FOR CRONTAB)

mkdir -p $SCRIPT_PATH && touch $SCRIPT_PATH/"$IP_ADDR"_ping.sh && { 
chown -R $USER $SCRIPT_PATH && chmod -R +x $SCRIPT_PATH 
}

#GENERATING EXEC SCRIPT
{
echo "ping -c 4 -w 15 -n $IP_ADDR"
echo "if [ \$? = 0 ]"
echo "then"
echo '	zenity --info --text "System '$IP_ADDR' has been pinged successfuly."'
echo "else"
echo '	zenity --error --text "Warning! System '$IP_ADDR' has not been pinged!"'
echo "fi"
} > $SCRIPT_PATH/$IP_ADDR_ping.sh

#ADDING SCRIPT TO CRONTAB

echo "$MINUTE $HOUR $DAY * * * sh $SCRIPT_PATH/"$IP_ADDR"_ping.sh" >> $CRONTAB_PATH

exit 0
#End of script