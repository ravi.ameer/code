#!/bin/bash
# Advanced system information


	#FUNCTIONS
	terminate() {
	 rm -f /home/$USER/sysinfo.log
	 zenity --error --text "Script terminated!"
	 exit 1
	}
	
	disk() {
	 {
	  echo "------------------Disk usage state------------------"
	  df -h | awk '{print $0}'
	  echo ""
	 } >> /home/$USER/sysinfo.log
	}

	sessions() {
	 {
	  echo "------------------Active sessions------------------"
	  who | awk '{print $0}'
	  echo ""
	 } >> /home/$USER/sysinfo.log
	}

	processes() {
	 {
	  echo "------------------Active processes------------------"
	  ps aux | awk '{print $1,$2,$8,$11}'
	  echo ""
	 } >> /home/$USER/sysinfo.log
	}

	memory(){
	 {
	  echo "------------------Memory usage state------------------"
	  free -m | awk '{print $0}'
	  echo ""
	 } >> /home/$USER/sysinfo.log	
	}

	#MAIN SCRIPT
	touch /home/$USER/sysinfo.log
	CHOOSE=$(zenity  --list  --text "Choose information to be added into log" --checklist  --column "Pick" --column "Option" TRUE "Disk usage state" TRUE "Active processes" TRUE "Active system sessions" TRUE "Memory usage state" --separator=" ")
	if [ $? != 0 ]
	then
		terminate
	fi

echo $CHOOSE > /home/$USER/sysinfo.tmp

grep -i disk /home/$USER/sysinfo.tmp
if [ $? = 0 ]
then
	disk
fi
grep -i sessions /home/$USER/sysinfo.tmp
if [ $? = 0 ]
then
	sessions
fi
grep -i memory /home/$USER/sysinfo.tmp
if [ $? = 0 ]
then
	memory
fi
grep -i processes /home/$USER/sysinfo.tmp
if [ $? = 0 ]
then
	processes
fi

rm -f /home/$USER/sysinfo.tmp

zenity --text-info --width=800 --height=1000 --filename=/home/$USER/sysinfo.log

rm -f /home/$USER/sysinfo.log
exit 0