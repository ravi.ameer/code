#!/bin/bash
# Zombie process catcher

ps aux | awk '{if ($8 = "Z") print $1,$2,$8,$11;}' | tee /home/$USER/zombie.tmp

ZCOUNT=$(wc -l /home/$USER/zombie.tmp | awk '{print $1}')
ZCOUNT=$(expr $ZCOUNT - 1)

zenity --warning --text="Total $ZCOUNT zombie processes found. Do you want to list them?"
if [ $? = 0 ]
then
zenity --text-info --title="Zombie processes in system" --filename=/home/$USER/zombie.tmp
fi`
rm -f /home/$USER/zombie.tmp
exit 0